#include "NvInfer.h"
#include "NvOnnxParser.h"
#include <cuda_runtime_api.h>

#include <iostream>
#include <fstream>

#include "engine.h"


using namespace nvinfer1;
using namespace nvonnxparser;

int main() {

    // Onnx path
    std::string modelFile = "/project/model.onnx";

    // create logger, network definition
    Logger logger;
    IBuilder* builder = createInferBuilder(logger);

    uint32_t flag = 1U << static_cast<uint32_t> (NetworkDefinitionCreationFlag::kEXPLICIT_BATCH); 
    INetworkDefinition* network = builder->createNetworkV2(flag);

    // create parser and parse onnx
    IParser* parser = createParser(*network, logger);
    
    parser->parseFromFile(
        modelFile.c_str(), 
        static_cast<int32_t>(ILogger::Severity::kWARNING)
    );
    for (int32_t i = 0; i < parser->getNbErrors(); ++i) {
        std::cout << parser->getError(i)->desc() << std::endl;
    }

    // create builder and build engine
    IBuilderConfig* config = builder->createBuilderConfig();
    config->setMemoryPoolLimit(MemoryPoolType::kWORKSPACE, 1U << 20);

    IHostMemory* serializedModel = builder->buildSerializedNetwork(*network, *config);

    // store model to disk
    std::ofstream p("engine.plan", std::ios::binary);
    p.write((const char*)serializedModel->data(),serializedModel->size());
    p.close();

    delete parser;
    delete network;
    delete config;
    delete builder;
    delete serializedModel;
    
    return 0;
}