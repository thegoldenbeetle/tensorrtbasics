#pragma once

#include "NvInfer.h"
#include <cuda_runtime_api.h>


// Class to extend TensorRT logger
class Logger : public nvinfer1::ILogger {
    void log(Severity severity, const char* msg) noexcept override;
};
