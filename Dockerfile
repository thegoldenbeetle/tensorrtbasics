# FROM nvidia/cuda:11.7.1-devel-ubuntu20.04
FROM nvcr.io/nvidia/pytorch:23.10-py3

ENV DUSER cv_user
ENV DPASSWORD cv_user
ENV PATH="/usr/local/cuda/bin:/home/${DUSER}/.local/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64:${LD_LIBRARY_PATH}"

# Install apt requirements
RUN apt-get update -y \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y sudo wget python3 python3-pip git vim tmux \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update -y \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y ffmpeg libsm6 libxext6 \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash ${DUSER} \ 
    && echo "${DUSER}:${DPASSWORD}" | chpasswd
RUN adduser ${DUSER} sudo

WORKDIR /project
RUN chown ${DUSER}:${DUSER} /project

USER ${DUSER}

RUN python3 -m pip install -U pip
RUN python3 -m pip install \
        --no-cache-dir \
        lightning torchmetrics tqdm seaborn pycuda

RUN git clone --recursive https://github.com/Microsoft/onnxruntime.git \
    && cd onnxruntime \
    && ./build.sh --use_cuda --cuda_home /usr/local/cuda --cudnn_home /usr/lib/x86_64-linux-gnu/ --config Release --build_shared_lib --compile_no_warning_as_error --skip_submodule_sync --skip_tests --build_wheel --parallel 2 --use_tensorrt --tensorrt_home /usr/lib/x86_64-linux-gnu/ \
    && python3 -m pip install \
       --no-cache-dir \
       build/Linux/Release/dist/*.whl

# ./build.sh --use_cuda --cuda_home /usr/local/cuda --cudnn_home /usr/lib/x86_64-linux-gnu/ --config Release --build_shared_lib --compile_no_warning_as_error --skip_submodule_sync --build_wheel --use_tensorrt --tensorrt_home /usr/lib/x86_64-linux-gnu/
# pip install  build/Linux/Release/dist/onnxruntime_gpu-1.17.0-cp310-cp310-linux_x86_64.whl
# pip install  build/Linux/Release/dist/*.whl

ENV PYTHONPATH "${PYTHONPATH}:/project"
ENV PYTHONPATH "${PYTHONPATH}:/project/src"