import lightning as L
import seaborn as sns
import torch
from torch import nn, optim
from torchmetrics import ConfusionMatrix, F1Score, Precision, Recall

from model.resnet import ResidualBlock, ResNet


class SimpleClassification(L.LightningModule):
    def __init__(self):
        super().__init__()
        self.num_classes = 200
        self.model = ResNet(ResidualBlock, num_classes=self.num_classes)
        self.criterion = nn.CrossEntropyLoss()

        self.f1 = F1Score(task="multiclass", num_classes=self.num_classes)
        self.precision = Precision(task="multiclass", num_classes=self.num_classes)
        self.recall = Recall(task="multiclass", num_classes=self.num_classes)
        self.confusion_matrix = ConfusionMatrix(
            task="multiclass", num_classes=self.num_classes
        )

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        inputs, labels = batch
        outputs = self.model(inputs)
        loss = self.criterion(outputs, labels)
        self.log("train/train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        inputs, labels = batch
        outputs = self.model(inputs)

        loss = self.criterion(outputs, labels)
        self.log("val/loss", loss)

        preds = torch.argmax(outputs, dim=1)

        f1_score = self.f1(preds, labels)
        self.log("val/f1_score", f1_score)

        precision_score = self.precision(preds, labels)
        self.log("val/precision_score", precision_score)

        recall_score = self.recall(preds, labels)
        self.log("val/recall_score", recall_score)

        if batch_idx == 0:
            cf_matrix = self.confusion_matrix(preds, labels)
            cf_plot = sns.heatmap(cf_matrix.detach().cpu(), annot=False)
            tensorboard = self.logger.experiment
            tensorboard.add_figure(
                "val/confusion_matrix",
                cf_plot.get_figure(),
                global_step=self.current_epoch,
            )

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        return optimizer
