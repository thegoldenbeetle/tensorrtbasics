import time
import argparse

import numpy as np
import onnxruntime as ort
from sklearn.metrics import f1_score
from tqdm import tqdm

from data.constants import DATA_DIR
from data.dataset import TinyImageNetDataset
from utils.utils import preprocess_pillow_image

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_onnx", type=str, help="filepath to model onnx file")
    args = parser.parse_args()

    ort_session = ort.InferenceSession(
        args.model_onnx, providers=["CUDAExecutionProvider"]
    )

    valid_dataset = TinyImageNetDataset(DATA_DIR, "val", transform=None)

    accuracy = 0
    total_time = 0

    gt_labels = []
    pred_labels = []
    for item in tqdm(valid_dataset):
        image, label = item

        start_time = time.time()

        image_array = preprocess_pillow_image(image)

        img_ortvalue = ort.OrtValue.ortvalue_from_numpy(image_array, "cuda", 0)

        io_binding = ort_session.io_binding()
        io_binding.bind_ortvalue_input("input", img_ortvalue)
        io_binding.bind_output("output")

        ort_session.run_with_iobinding(io_binding)

        ort_outs = io_binding.copy_outputs_to_cpu()
        pred_label_idx = np.argmax(ort_outs)

        total_time += time.time() - start_time
        accuracy += pred_label_idx == label

        gt_labels.append(label)
        pred_labels.append(pred_label_idx)

    print("Accuracy: ", accuracy / len(valid_dataset))
    print("F1 score: ", f1_score(gt_labels, pred_labels, average="macro"))
    print("Mean time, ms : ", total_time / len(valid_dataset) * 1000)
