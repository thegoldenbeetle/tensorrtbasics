import lightning as L
import torch
from lightning.pytorch.callbacks import ModelCheckpoint
from torchvision import transforms

from data.constants import TRAINING_BATCH_SIZE, DATA_DIR
from data.dataset import TinyImageNetDataset, normalize
from model.model import SimpleClassification

if __name__ == "__main__":
    valid_transforms = transforms.Compose([transforms.ToTensor(), normalize])

    valid_dataset = TinyImageNetDataset(DATA_DIR, "val", valid_transforms)
    valid_loader = torch.utils.data.DataLoader(
        valid_dataset, batch_size=TRAINING_BATCH_SIZE, shuffle=False, num_workers=4
    )

    train_transforms = transforms.Compose(
        [
            transforms.RandomResizedCrop(64, scale=(0.8, 1.0)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.RandomRotation(10),
            transforms.ToTensor(),
            normalize,
        ]
    )

    train_dataset = TinyImageNetDataset(DATA_DIR, "train", train_transforms)
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=TRAINING_BATCH_SIZE, shuffle=True, num_workers=4
    )

    classification_model = SimpleClassification()

    checkpoint_callback = ModelCheckpoint(save_top_k=1, monitor="val/loss", mode="min")

    trainer = L.Trainer(
        devices=1, accelerator="gpu", max_epochs=30, callbacks=[checkpoint_callback]
    )
    trainer.fit(
        model=classification_model,
        train_dataloaders=train_loader,
        val_dataloaders=valid_loader,
    )
