import time
import argparse

import numpy as np
import torch
from sklearn.metrics import f1_score
from torchvision import transforms
from tqdm import tqdm

from data.constants import DATA_DIR
from data.dataset import TinyImageNetDataset, normalize
from model.model import SimpleClassification

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--model_ckpt", type=str, help="filepath to model checkpoints")
    args = parser.parse_args()

    simple_model = SimpleClassification.load_from_checkpoint(args.model_ckpt).eval()

    valid_dataset = TinyImageNetDataset(DATA_DIR, "val", transform=None)

    accuracy = 0
    total_time = 0

    gt_labels = []
    pred_labels = []
    for item in tqdm(valid_dataset):
        image, label = item

        start_time = time.time()

        image_tensor = transforms.ToTensor()(image)
        image_tensor = normalize(image_tensor)
        image_tensor = torch.unsqueeze(image_tensor, 0).to(device)

        preds = simple_model(image_tensor)
        pred_label_idx = np.argmax(preds.detach().cpu().numpy())

        total_time += time.time() - start_time

        accuracy += pred_label_idx == label

        gt_labels.append(label)
        pred_labels.append(pred_label_idx)

    print("Accuracy: ", accuracy / len(valid_dataset))
    print("F1 score: ", f1_score(gt_labels, pred_labels, average="macro"))
    print("Mean time, ms : ", total_time / len(valid_dataset) * 1000)
