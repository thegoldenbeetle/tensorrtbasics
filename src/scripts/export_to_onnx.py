import argparse

import torch

from model.model import SimpleClassification

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--ckpt", type=str, help="lightning checkpoints file")
    parser.add_argument("--output", type=str, help="output onnx model filepath")
    args = parser.parse_args()

    simple_model = SimpleClassification.load_from_checkpoint(args.ckpt).eval()
    onnx_filepath = args.output

    input_sample = torch.randn((1, 3, 64, 64))
    simple_model.to_onnx(
        onnx_filepath,
        input_sample,
        export_params=True,            # store the trained parameter weights inside the model file
        input_names=["input"],         # the model's input names
        output_names=["output"],       # the model's output names
    )
