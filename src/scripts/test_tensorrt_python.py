import time
import argparse

import pycuda.driver as cuda
import pycuda.autoinit

import numpy as np
import tensorrt as trt
from sklearn.metrics import f1_score
from tqdm import tqdm

from data.constants import DATA_DIR
from data.dataset import TinyImageNetDataset
from utils.utils import preprocess_pillow_image

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--engine", type=str, help="filepath to tensorrt engine file")
    parser.add_argument("--precision", type=str, default="float32", help="target precision")
    args = parser.parse_args()

    # runtime interface used for engine deserialization
    runtime = trt.Runtime(trt.Logger(trt.Logger.WARNING))

    # load engine from file, engine holds the optimized model
    with open(args.engine, "rb") as f:
        engine = runtime.deserialize_cuda_engine(f.read())
    context = engine.create_execution_context()
    
    target_precision = args.precision

    # allocate memory for model output
    output = np.empty([1, 200], dtype=target_precision)
    d_output = cuda.mem_alloc(1 * output.nbytes)
    
    # allocate memory for model input
    input = np.empty([1, 3, 64, 64], dtype=target_precision)
    d_input = cuda.mem_alloc(1 * input.nbytes)
    
    # pointers for tensorrt to this allocated memory
    bindings = [int(d_input), int(d_output)]

    stream = cuda.Stream()
    
    valid_dataset = TinyImageNetDataset(DATA_DIR, "val", transform=None)

    accuracy = 0
    total_time = 0
    gt_labels = []
    pred_labels = []

    for item in tqdm(valid_dataset):
        image, label = item

        start_time = time.time()

        # preprocess pillow image to input format of the model
        # includes image normalization and reshaping
        image_array = preprocess_pillow_image(image).astype(target_precision)
        image_array = np.ascontiguousarray(image_array)

        # transfer input data to device
        cuda.memcpy_htod_async(d_input, image_array, stream)

        # execute model
        context.execute_async_v2(bindings, stream.handle, None)

        # transfer predictions back
        cuda.memcpy_dtoh_async(output, d_output, stream)
        pred_label_idx = np.argmax(output[0])

        # syncronize threads
        stream.synchronize()

        # calcuate statistics
        total_time += time.time() - start_time
        accuracy += pred_label_idx == label
        gt_labels.append(label)
        pred_labels.append(pred_label_idx)

    print("Accuracy: ", accuracy / len(valid_dataset))
    print("F1 score: ", f1_score(gt_labels, pred_labels, average="macro"))
    print("Mean time, ms : ", total_time / len(valid_dataset) * 1000)
