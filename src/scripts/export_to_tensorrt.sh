#! /bin/bash

trtexec --onnx=model.onnx --saveEngine=model.plan --inputIOFormats=fp16:chw --outputIOFormats=fp16:chw --fp16

# --onnx - onnx model filepath
# --saveEngine - filepath to save plan file (serialized engine)
# --inputIOFormats - type and format of each of the input tensors
# --outputIOFormats - type and format of each of the output tensors
#                     precision:dimensions_order (c - channels, h - height, w - width)
# --fp16 - enable fp16 precision
# batch size will be determined from the onnx file

# To see all possible options run `trtexec -h`
