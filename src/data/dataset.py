import os

from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms

normalize = transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))


class LabelEncoder:
    def __init__(self, root_dir):
        labels_file_path = os.path.join(root_dir, "wnids.txt")
        with open(labels_file_path, "r") as f:
            class_ids = f.read().strip().split("\n")
        self.classes = sorted(list(set(class_ids)))
        self.classes_dict = {label: idx for idx, label in enumerate(self.classes)}

    def label_to_idx(self, label):
        return self.classes_dict[label]

    def idx_to_label(self, idx):
        return self.classes[idx]


class TinyImageNetDataset(Dataset):
    def __init__(self, root_dir, split="train", transform=None):
        self.root_dir = root_dir
        self.split = split
        self.transform = transform
        self.label_encoder = LabelEncoder(root_dir)

        self.data = []
        self.labels = []

        if self.split == "train":
            with open(os.path.join(root_dir, "wnids.txt"), "r") as f:
                class_ids = f.read().strip().split("\n")
            for class_id in class_ids:
                class_dir = os.path.join(root_dir, "train", class_id, "images")
                for filename in os.listdir(class_dir):
                    self.data.append(os.path.join(class_dir, filename))
                    self.labels.append(self.label_encoder.label_to_idx(class_id))

        elif self.split == "val":
            with open(os.path.join(root_dir, "val", "val_annotations.txt"), "r") as f:
                for line in f:
                    parts = line.strip().split("\t")
                    image_path = os.path.join(root_dir, "val", "images", parts[0])
                    class_id = parts[1]
                    self.data.append(image_path)
                    self.labels.append(self.label_encoder.label_to_idx(class_id))

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        image_path = self.data[idx]
        label = self.labels[idx]

        image = Image.open(image_path).convert("RGB")
        if self.transform:
            image = self.transform(image)

        return image, label
