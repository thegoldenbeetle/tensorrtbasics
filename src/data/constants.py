import os

DATA_DIR = "tiny-imagenet-200"  # Original images come in shapes of [3,64,64]

TRAIN_DIR = os.path.join(DATA_DIR, "train")
VALID_DIR = os.path.join(DATA_DIR, "val")

TRAINING_BATCH_SIZE = 200
