import numpy as np


def preprocess_pillow_image(image):
    # imagenet stats
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]

    # convert to array
    pixels = np.asarray(image).astype(np.float32)
    pixels = pixels / 255.0

    # normalize
    pixels = (pixels - mean) / std

    # shape to 1xcxwxh
    pixels = np.moveaxis(pixels, -1, 0)
    pixels = np.expand_dims(pixels, axis=0)
    return pixels.astype(np.float32)
